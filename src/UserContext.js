import React from 'react';

const UserContext = React.createContext();

// "provider" - allows other components to consume/use the context object and supply ng necessary information need to the context object.
export const UserProvider = UserContext.Provider;

export default UserContext;